import numpy as np
import imutils
import cv2

class VideoStream:
    def __init__(self, video_source=0):
        self.vidcap = cv2.VideoCapture(video_source)
        self.mode_dict = {'RETR_EXTERNAL' : cv2.RETR_EXTERNAL, \
                            'RETR_TREE' : cv2.RETR_TREE, \
                            'RETR_LIST' : cv2.RETR_LIST, \
                            'RETR_CCOMP' : cv2.RETR_CCOMP \
                            }
        self.method_dict = {'CHAIN_APPROX_NONE' : cv2.CHAIN_APPROX_NONE, \
                            'CHAIN_APPROX_SIMPLE' : cv2.CHAIN_APPROX_SIMPLE, \
                            'CHAIN_APPROX_TC89_L1' : cv2.CHAIN_APPROX_TC89_L1, \
                            'CHAIN_APPROX_TC89_KCOS' : cv2.CHAIN_APPROX_TC89_KCOS \
                             }
        if not self.vidcap.isOpened():
            raise ValueError("Unable to open video source:", video_source)

        self.width = self.vidcap.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vidcap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def __del__(self):
        if self.vidcap.isOpened():
            self.vidcap.release()

    def get_frame(self, options):
        if self.vidcap.isOpened():
            ret, frame = self.vidcap.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            #Apply options
            if options['bnw'].get():
                frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            if options['vMirror']:
                frame = cv2.flip(frame, 0)
            if options['hMirror']:
                frame = cv2.flip(frame, 1)
            if options['Blur'].get():
                frame = cv2.GaussianBlur(frame, (options['BlurKernelh'].get() + 1, options['BlurKernelw'].get() + 1), options['BlurDeviationX'].get(), options['BlurDeviationY'].get())
            if not options['Contrast'].get() == 0 or not options['Brightness'].get() == 0:
                frame = self.adjust_BC(frame, options['Brightness'].get(), options['Contrast'].get())
            if ret:
                frame = imutils.rotate_bound(frame, options['Rotate'] * 90)
                if options['EdgeDetection'].get():
                    edges = cv2.Canny(frame, options['EdgeDetectionThresh1'].get(), options['EdgeDetectionThresh2'].get())
                    cnts = cv2.findContours(edges.copy(), self.mode_dict[options['EdgeDetectionMode'].get()], self.method_dict[options['EdgeDetectionMethod'].get()])
                    cnts = imutils.grab_contours(cnts)
                    cv2.drawContours(frame, cnts, -1, tuple(options['EdgeDetectionColour']), options['EdgeDetectionThickness'].get())
                for circleObjs in options['PaintArray']:
                    for circleObj in circleObjs:
                        cv2.circle(frame, circleObj['coords'], circleObj['size'], circleObj['colour'], -1)
                for circleObj in options['LastPaintArray']:
                    cv2.circle(frame, circleObj['coords'], circleObj['size'], circleObj['colour'], -1)
                return (ret, frame)
            else:
                return (ret, None)
        else:
            return (ret, None)

    def adjust_BC(self, input_img, brightness, contrast):
        if brightness != 0:
            if brightness > 0:
                shadow = brightness
                highlight = 255
            else:
                shadow = 0
                highlight = 255 + brightness
            alpha_b = (highlight - shadow)/255
            gamma_b = shadow

            buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
        else:
            buf = input_img.copy()

        if contrast != 0:
            f = 131*(contrast + 127)/(127*(131-contrast))
            alpha_c = f
            gamma_c = 127*(1-f)

            buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

        return buf
